/*
 *  JuceApp.h
 *  sdaProj
 *
 */

#ifndef H_JUCEAPP
#define H_JUCEAPP

#include <juce.h>
#include "AppWindow.h"

//==============================================================================
class JuceApp  : public JUCEApplication
{
public:
    //==========================================================================
    JuceApp();
    ~JuceApp();
    //==========================================================================
    void initialise (const String& commandLine);
    void shutdown();
    //==========================================================================
    void systemRequestedQuit();
    //==========================================================================
    const String getApplicationName();
    const String getApplicationVersion();
    bool moreThanOneInstanceAllowed();
    void anotherInstanceStarted(const String& commandLine);
	
private:
    AppWindow* appWindow;
};


#endif //H_JUCEAPP