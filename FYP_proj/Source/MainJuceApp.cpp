/*
 *  JuceApp.cpp
 *  sdaProj
 *
 */

#include "MainJuceApp.h"

JuceApp::JuceApp() : appWindow (0)
{
	// Don't do anything in this constructor! It will be called before the
	// main Juce subsystem has been initialised!

	// This is where the application itself is created. It's a bit like a shell, which
	// all your real app stuff gets born into. At this point (where the app pops into
	// being), Juce doesn't really 'exist' yet, so we can't do anything with it here.
	// Once this application shell has been established, Juce will be awake so we can
	// create instances of its classes willy-nilly.
	
	// [Jules says...]
	// NEVER do anything in here that could involve any Juce function being called
	// - leave all your startup tasks until the initialise() method.
}

JuceApp::~JuceApp()
{
	// Don't do anything in this destructor! It will be called after the
	// main Juce subsystem has been shutdown and is no longer valid!
	
	// This is where the application body is destroyed, and making any Juce calls in
	// here could be very dangerous.
	
	// Just as Juce wasn't awake when the shell was created, by this stage it's been
	// tidied away and put to sleep. This is the bit that happens AFTER your program
	// code has ended (and hopefully put all its toys away).
	
	// [Jules says...]
	// Your shutdown() method should already have done all the things necessary to
	// clean up this app object, so you should never need to put anything in
	// the destructor.
}

//==============================================================================
void JuceApp::initialise(const String& commandLine)
{
	// Do your application's initialisation code here..
	
	// This is called automatically when the application is ready to launch.
	// So far, it just exists in memory as an empty pocket of potential waiting
	// to burst into life as a program. Nothing yet exists to act or be displayed.
	
	// All we want to do here is create the main window. This instantiates an object
	// of 'MainAppWindow' - which we have defined in MainAppWindow(.h/.cpp). The app's
	// behaviour comes from that, so all we need is to bring it to life...
	appWindow = new AppWindow();
	// ... and plonk it onto the display...
	appWindow->centreWithSize(600, 620);   // [*] (see below for a tip on this)
	// ... (of course making sure that it is visible!)
	appWindow->setVisible(true);
	
	// That's all we have to do here. Once this function has ended, the Juce
	// application will start firing its event loop. This is basically the
	// engine that powers the Juce app classes (giving life to the Component
	// and messaging model), and is something that will just happen by itself.
	
	// The event dispatch loop will keep the app alive until something calls
	// JUCEApplication::quit() - which could be windows closing the application,
	// or the user clicking the window's close button.
	
	// [*] When you set the size of the window, there is something important to bear
	//     in mind: The dimensions you set here are for the window component itself.
	//     If this is a DocumentWindow (which is the most obvious thing for it to be),
	//     then it will have a titlebar and a border. The content component sits inside
	//     this arrangement of parts - which means that your program's main component
	//     dimensions will actually be slightly smaller than the values you set here.
	//     This only matters if you're relying on positioning your program's various
	//     widgets using constant values instead of calculating relative positions from
	//     the available dimensions (i.e. saying 'this button is 50 pixels wide' instead
	//     of 'this button is 1/3 of the width of its parent component').
}

void JuceApp::shutdown()
{
	// Do your application's shutdown code here..
	
	// This gets called when the application is ready to shut down.
	// Anything that we created in the initialise() function should be destroyed,
	// so that nothing is left hanging around when the app shell ceases to exist.
	
	// All we need to do here is delete the MainAppWindow we created...
	deleteAndZero (appWindow);
}

//==============================================================================
void JuceApp::systemRequestedQuit()
{
	quit();
}

//==============================================================================
const String JuceApp::getApplicationName()
{
	// The name for the application
	return PROJECT_NAME;
}

const String JuceApp::getApplicationVersion()
{
	// Here we can give a 'version' indicator, to distinguish one build from
	// another. If you update your program, it's a good idea to also update
	// the string returned here.
	
	return "0.0.1";
}

bool JuceApp::moreThanOneInstanceAllowed()
{
	// We can prevent multiple instances of the application here by returning false.
	return true;
}

void JuceApp::anotherInstanceStarted(const String& commandLine)
{
	// This will get called if the user launches another copy of the application.
}

//==============================================================================
// This macro generates the main() routine that starts the app.
START_JUCE_APPLICATION(JuceApp)