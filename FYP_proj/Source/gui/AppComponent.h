/*
 *  AppComponent.h
 *  sdaProj
 *
 */

#ifndef H_APPCOMPONENT
#define H_APPCOMPONENT

#include <juce.h>

class AppComponent  : public Component, public ButtonListener
{	
public:
	//==============================================================================
	AppComponent();
	~AppComponent();
	//==============================================================================
	void resized();
	void paint (Graphics &g);
	void buttonClicked (Button* button);
	//==============================================================================
private:
	//==============================================================================
	//Add data members here
	
	TextButton button1;
	Label someText;

};




#endif // H_APPCOMPONENT