/*
 *  JuceWindow.h
 *  sdaProj
 *
 */

#ifndef H_APPWINDOW
#define H_APPWINDOW

#include <juce.h>

class AppWindow  : public DocumentWindow
{
public:
	//==============================================================================
	AppWindow();
	~AppWindow();
	//==============================================================================
	// called when the close button is pressed or esc is pushed
	void closeButtonPressed();
	
};



#endif //H_APPWINDOW