/*
 *  AppComponent.cpp
 *  sdaProj
 *
 */

#include "AppComponent.h"

AppComponent::AppComponent()
{
	// This is where the app component is created, so we initialise and
	// configure it according to our needs.
	
	// One thing that covers is creating any widgets and components we want to
	// display. Also, if any widgets will need responding to, we must hook them up
	// to their listeners here too (and it's likely that this class itself will be
	// the listener in question, providing we've inherited the appropriate class!)
	
	// Add your component initialisation code here!
	
	button1.setButtonText("Click Me!");
	addAndMakeVisible(&button1);
	
	someText.setText("I say something...", false);
	addAndMakeVisible(&someText);
	
	button1.addListener(this);

}
AppComponent::~AppComponent()
{
	// Be sure to destroy any objects you've created using 'new' here. If your objects
	// are on the stack (i.e. they were created without pointers or the 'new' operator,
	// then they die automatically. If you've created them
	// manually on the heap (for example, if you've got a pointer and you've created a new
	// object for it) then it must be deleted.
}

//==============================================================================
void AppComponent::resized()
{
	// This is called whenever this component's size changes. We could respond
	// to this in a number of ways, but the most obvious thing to do is reposition
	// all our widgets, using their 'setBounds()' function.
	// It's nice to position them relative to the size of this Component. That means
	// making use of the 'getWidth()' and 'getHeight()' functions to determine where
	// to put them and how big they should be.
	
	button1.setBounds(20, 20, getWidth() - 40, 20); // (position from left, position from top, width, height)
	
	someText.setBounds(20, 50, getWidth() - 40, 20);
	
}

void AppComponent::paint (Graphics &g)
{

}

void AppComponent::buttonClicked (Button* button)
{
	someText.setText("You Clicked the button!", false);
}

