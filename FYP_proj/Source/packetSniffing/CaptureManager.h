/*
 *  CaptureManager.h
 *  fyp_proj
 *
 *  Created by Connor Smout on 28/10/2013.
 *  Copyright 2013. All rights reserved.
 *
 */

#ifndef H_CAPTUREMANAGER
#define H_CAPTUREMANAGER

#include <juce.h>
#include <pcap.h>

class CaptureManager
{
public:
	//==============================================================================
	CaptureManager();
	~CaptureManager();
	//==============================================================================
	
	//Modifiers----------------------------
	
	void setDev(char devName);
	
	//Accessors----------------------------
	
	/**
	 Function that returns a pointer to a list of avalable devices
	 */
	pcap_if_t* getDevList(void); // Is this conventional to return a stucture??	
	
	
private:
	
	char *dev;
	pcap_if_t *alldevs; 
	
	char errbuf[PCAP_ERRBUF_SIZE]; // To be populated with error messages
	

	
};



#endif //H_CAPTUREMANAGER